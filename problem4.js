// ==== Problem #4 ====
// The accounting team needs all the years from every car on the lot.
//Execute a function that will return an array from the dealer data containing only the car years and log the result in the console as it was returned.


let problem4 = function (inventory = []) {
    let length = inventory.length
    if (length === 0) {
        return []
    }
    else {
        let array1 = []
        for (c = 0; c < length; c++) {
            let file = inventory[c];
            array1.push(file.car_year);
        }
        return array1;
    }

}

module.exports = problem4