const inventory = require('../cars.js');
const solution = require('../problem1');
id = 33
let solution1 = solution(inventory, id);


if (solution1.length !== 0) {
    let result = `Car ${solution1.id} is a ${solution1.car_year} ${solution1.car_make} ${solution1.car_model}`
    console.log({ result });
}
else {
    console.log(solution1);
}
