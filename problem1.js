// ==== Problem #1 ====
// The dealer can't recall the information for a car with an id of 33 on his lot. Help the dealer find out which car has an id of 33 by calling a function
//  that will return the data for that car.
//  Then log the car's year, make, and model in the console log in the format of: 
// "Car 33 is a *car year goes here* *car make goes here* *car model goes here*"



let problem1 = function (inventory = [], id = undefined) {
    if ((id === undefined) || (inventory.length === 0)) {
        return []
    }
    else {
        let lengthOfFile = inventory.length;
        let checkId = false
        for (c = 0; c < lengthOfFile; c++) {
            file = inventory[c]
            if (file.id === id) {
                checkId = true
                return file;
            }
        };
        if (!checkId) {
            return [];
        }
    }
};

module.exports = problem1