// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website.
//Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.

const { __esModule } = require("async");

let problem3 = function (inventory = []) {
    let length = inventory.length
    if (length === 0) {
        return []
    }
    else {
        let array1 = []
        for (c = 0; c < length; c++) {
            let file = inventory[c];
            array1.push(file.car_model);
        }
        return array1;
    }
    
}

module.exports = problem3