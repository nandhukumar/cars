// ==== Problem #6 ====
// A buyer is interested in seeing only BMW and Audi cars within the inventory.
// Execute a function and return an array that only contains BMW and Audi cars.
//Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.


let problem6 = function (file1, model) {
    if ((!file1) || (!model)) {
        return []
    }
    else {
        let array1 = []
        let lengthOfFile = file1.length;
        let lengthOfModel = model.length;
        for (c = 0; c < lengthOfFile; c++) {
            let file = file1[c];
            for (k = 0; k < lengthOfModel; k++) {
                if (file.car_make === model[k]) {
                    array1.push(file);
                }
            }

        }
        return array1;
    }

}

module.exports = problem6