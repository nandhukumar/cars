// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000.
//Using the array you just obtained from the previous problem,
//find out how many cars were made before the year 2000 and return the array of older cars and log its length.


let problem5 = function (inventory = []) {
    let length = inventory.length
    if (length === 0) {
        return []
    }
    else {
        let array1 = []
        for (c = 0; c < length; c++) {
            let file = inventory[c];
            if (parseInt(file.car_year) < 2000) {
                array1.push(file);
            }
        }
        return array1.length;
    }

}

module.exports = problem5